﻿using UnityEngine;
using System.Collections;

public class CharacterMovement : MonoBehaviour {



    private Animator animator;
    private CharacterController controller;

    private float speed = 1;
    private float transitionTime = 0.2F; //transition between animation

    public float boost = 8;

	// Use this for initialization
	void Start () {
        controller = GetComponent<CharacterController>();
        animator = GetComponent<Animator>();

	}
	
	// Update is called once per frame
	void Update () {


        if (controller.isGrounded) //character is on the ground
        {
            
            //get directional axis
            float h = Input.GetAxis("Horizontal"); 
            float v = Input.GetAxis("Vertical");

            //allow sprint
            if (Input.GetKey(KeyCode.Joystick1Button4) || Input.GetKey(KeyCode.Joystick1Button5)) //run
            {
                speed = Mathf.Sqrt(h*h+v*v) * boost;
            }
            else
                speed = Mathf.Sqrt(h*h+v*v);

            //transform direction to screen
            Vector3 axis = Vector3.Cross(transform.root.forward, new Vector3(h,0,v));
            float d = Vector3.Angle(transform.root.forward, new Vector3(h,0,v)) / 180.0f * (axis.y < 0 ? -1 : 1);
            
            //send to the animator
            animator.SetFloat("Speed", speed,transitionTime, Time.deltaTime);
            animator.SetFloat("Direction",d);
            

            //transform.Rotate(Vector3.up * (Time.deltaTime * v * Input.GetAxis("Mouse X") * 90), Space.World);
        }


	}
}
